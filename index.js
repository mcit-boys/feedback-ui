const ratingsEls = document.querySelectorAll(".ratings");

let selectingRatings="";

const btnEl = document.getElementById("btn");

const containerEl = document.getElementById("container");

ratingsEls.forEach((ratingsEl)=> { 
    ratingsEl.addEventListener("click", (event)=> {
    removeActive();
    selectingRatings = event.target.innerText || event.target.parentNode.innerText;
    event.target.classList.add("active"); 
    event.target.parentNode.classList.add("active");

})
});
btnEl.addEventListener("click", ()=>{
    if(selectingRatings !=="")
    {
        containerEl.innerHTML = `
        <strong>Thank you!</strong>
        <br>
        <br>
        <strong>Feedback: ${selectingRatings}</strong>
        <p>We'll use your Feedback to improve our Customer support</p>
        `
    }
})

function removeActive(){
    ratingsEls.forEach((ratingsEl)=>{
        ratingsEl.classList.remove("active");
    })
}